($ => {
  "use strict";
  const $navbar = $(".nav");
  //scroll top 0 sticky
  $(document).scroll(() => {
    var scrollTop = $(this).scrollTop();
    if (scrollTop > 0) {
      $navbar.addClass("is-sticky")
    } else {
      $navbar.removeClass("is-sticky")
    }
  })
})(jQuery, undefined)
let slideShow = true
$(".nav__menu").click(() => {
  $(".nav").toggleClass("is-open")
  $("body").toggleClass("fixed-body")
})

$(document).ready(() => {
  start_slideshow()
  sizeTheVideo()
  $(window).resize(() => {
    sizeTheVideo()
  })
  const slideButton = $("#button__slide--play")
  slideButton.click(() => {
    if(slideShow) {
      pause_slideshow()
      slideButton.find('div').first().text("Play")
    }
    else {
      start_slideshow()
      slideButton.find('div').first().text("Pause")
    }
  })
})

let interval

start_slideshow = () => {
  slideShow = true
  clearInterval(interval)
  const images = ["zermatt", "carousal-02", "carousal-03"]
  interval = setInterval(() => {
    const img_no = document.getElementById("image_no").value
    $('#hero__bg').css("background-image", `url(./assets/images/${images[img_no]}.jpg)`)
    if (img_no == 2) 
      document.getElementById("image_no").value = 0
    else 
      document.getElementById("image_no").value = Number(img_no) + 1
  }, 3000)
}

pause_slideshow = () => {
  slideShow = false
  clearInterval(interval)
}

const onPlayerReady = (event) => {
  event.target.playVideo()
}

onYouTubeIframeAPIReady = () => {
  new YT.Player('player', {
    width: "100%",
    videoId: 't3KrMH57Sl4',
    playerVars: {
      color: 'white',
    },
    events: {
      'onReady': onPlayerReady
    }
  })
}

const sizeTheVideo = () => {
  const aspectRatio = 1.78
  const video = $('#videoWithJs iframe')
  const card = $("#video__card")
  const newHeight = card.width() / aspectRatio
  video.css({ "width": "100%", height: `${newHeight}px` })
}

